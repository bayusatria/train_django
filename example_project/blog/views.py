from django.shortcuts import render
from django.http import HttpResponse 
from blog.models import Posts

# posts = [
#     {
#         'author': 'Bayu Satria Gemilang',
#         'title': 'Blog Post 1',
#         'content': 'First post content',
#         'date_posted': 'September 14, 2019'
#     },
#     {
#         'author': 'Elsa Susanty',
#         'title': 'Blog Post 2',
#         'content': 'Second post content',
#         'date_posted': 'September 14, 2019'
#     }
# ]

def home(request):
    context = {
        'posts': Posts.objects.all()
    }
    return render(request, 'blog/home.html', context)

def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})
